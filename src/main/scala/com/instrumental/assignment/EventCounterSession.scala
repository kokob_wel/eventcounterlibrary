package com.instrumental.assignment

import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.util
import java.util.concurrent.{ConcurrentLinkedQueue, Executors}
import java.util.logging.Logger
import java.util.stream.Collectors
import com.instrumental.assignment.config.LibConfig
import com.instrumental.assignment.model.EventMetaData

/**
 * Created by KOKOB WELDETENSAE on 8/26/20.
 * Event counter session singleton object that stores,monitors and provide insights about events.
 */
object EventCounterSession extends AutoCloseable{

  var eventStorage: ConcurrentLinkedQueue[EventMetaData] = new ConcurrentLinkedQueue[EventMetaData]
  val retentionPeriodInMinutes: Int = LibConfig.retentionMinutes
  val retentionPeriodInSeconds: Int = retentionPeriodInMinutes*60
  val logger:Logger = Logger.getLogger("EventCounterSession")

  private val custodianExecutorService = Executors.newFixedThreadPool(1)

  /**
   * An independent thread executor that is responsible to clean events stored more than configured retention period.
   */
  custodianExecutorService.execute(()=>{
    while(true) {
      Thread.sleep(retentionPeriodInSeconds*1000)
      logger.info("cleaning storage")
      val rightNow = LocalDateTime.now()
      val rightThen = rightNow.minus(retentionPeriodInMinutes, ChronoUnit.MINUTES)
      if(!eventStorage.isEmpty)
          eventStorage.removeIf(event => event.timeStamp.isBefore(rightThen))
    }
  })

  /**
   * Method that triggers an event
   * @param eventName optional parameter to indicate the name of the event.
   */
  def signalEvent(eventName:String = "Default event name"): Unit = {
      eventStorage.add(EventMetaData(LocalDateTime.now(),eventName))
  }

  /**
   * Method that serves as an end point to get the count of all events that occurred during the last given seconds.
   * @param timeInSeconds given time in seconds.
   * @return
   */
  def getEventCount(timeInSeconds:Int):Int = {
    validateSupportedTimeSpan(timeInSeconds)
    getEvents(timeInSeconds).size
  }

  /**
   * A method that serves as an end point to get all events that occurred during the last given seconds.
   * @param timeInSeconds given seconds.
   * @return ListBuffer of all EventMetaData objects that are registered during the last given seconds.
   */
  def getEvents(timeInSeconds: Int): util.List[EventMetaData] = {
    validateSupportedTimeSpan(timeInSeconds)
    val rightNow = LocalDateTime.now()
    val rightThen = rightNow.minus(timeInSeconds,ChronoUnit.SECONDS)
    eventStorage.stream().filter(event=>event.timeStamp.isAfter(rightThen)).collect(Collectors.toList[EventMetaData])
  }

  /**
   * A method that is responsible to validate the time span requested.
   * @param timeInSeconds requested time span to read or get counts of events.
   * @throws java.lang.IllegalArgumentException exception thrown as a result of invalid time span.
   */
  @throws(classOf[IllegalArgumentException])
  def validateSupportedTimeSpan(timeInSeconds: Int) : Unit = {
      if(timeInSeconds > retentionPeriodInSeconds) {
         val errorMessage = s"Requested time span is not supported. Requested time span exceeds $retentionPeriodInMinutes minutes."
         throw new IllegalArgumentException(errorMessage)
      }
  }

  /**
   * A method to delete all registered events.
   */
  def resetCounter(): Unit = {
     eventStorage.clear()
  }

  override def close(): Unit = {
    resetCounter()
    custodianExecutorService.shutdown()
  }
}
