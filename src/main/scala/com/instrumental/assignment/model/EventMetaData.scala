package com.instrumental.assignment.model

import java.time.LocalDateTime

/**
 * Created by KOKOB WELDETENSAE on 8/26/20.
 * A case class to represent an event.
 * @param timeStamp the time during which the event was created.
 * @param eventName name of the event.
 */
case class EventMetaData(timeStamp: LocalDateTime, eventName: String)
