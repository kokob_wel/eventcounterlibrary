# README #

### What is this repository for? ###

* A project that is designed to serve as an event counter library. Written in scala and sbt build tool.
* Version - 0.0.1

### How do I get set up? ###

* Clone this project in to your local development environment.
* You may configure retention period of the event storage mechanism in reference.conf or you may override the value via application.conf 
* Dependencies - TypeSafe , Scalatest
* How to run tests - Once imported the project locally, run 'sbt compile' and then 'sbt test'


### Who do I talk to? ###

* Repo owner or admin -  email kobinet920@gmail.com