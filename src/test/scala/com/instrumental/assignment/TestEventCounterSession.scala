package com.instrumental.assignment

import org.scalatest.BeforeAndAfter
import org.scalatest.funsuite.AnyFunSuite

class TestEventCounterSession  extends AnyFunSuite  with BeforeAndAfter {

  var eventCounter:EventCounterSession.type = _

  before{
    eventCounter = EventCounterSession
  }

  after{
    eventCounter.resetCounter()
  }

  test("test event counter session"){
    eventCounter.signalEvent("Event 1")
    Thread.sleep(1000)
    eventCounter.signalEvent("Event 2")
    Thread.sleep(1000)
    eventCounter.signalEvent("Event 3")
    eventCounter.signalEvent("Event 4")
    eventCounter.signalEvent("Event 5")
    assert(eventCounter.getEventCount(1)==3)
    assert(eventCounter.getEventCount(2)==4)
  }

  test("test reset counter"){
    eventCounter.signalEvent("Event 1")
    eventCounter.signalEvent("Event 2")
    eventCounter.signalEvent("Event 3")
    assert(eventCounter.getEventCount(1) == 3)
    eventCounter.resetCounter()
    assert(eventCounter.getEventCount(1) == 0)
  }

  test("test getEvents") {
    val firstEventName = "Event 101"
    val secondEventName = "Event 102"
    val thirdEventName = "Event 103"
    eventCounter.signalEvent("Event 101")
    eventCounter.signalEvent("Event 102")
    eventCounter.signalEvent("Event 103")
    val allEvents = eventCounter.getEvents(1)
    assert(allEvents.size == 3)
    assert(allEvents.get(0).eventName == firstEventName)
    assert(allEvents.get(1).eventName == secondEventName)
    assert(allEvents.get(2).eventName == thirdEventName)
  }

}
