name := "eventcounterlibrary"

version := "0.0.1"

scalaVersion := "2.13.1"

libraryDependencies += "com.typesafe" % "config" % "1.3.3"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.1.1" % Test