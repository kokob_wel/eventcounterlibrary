package com.instrumental.assignment.config

import com.typesafe.config.{Config, ConfigFactory}

/**
  * Created by KOKOB WELDETENSAE on 8/26/20.
  * Configuration instance that captures config value specified in reference.conf or application.conf.
  */
object LibConfig {

  val config: Config = ConfigFactory.load()
  val root: Config = config.getConfig("EventCounterConfig")
  val retentionConfig: Config = root.getConfig("retention")

  val retentionMinutes: Int = retentionConfig.getInt("time-in-minutes")
}